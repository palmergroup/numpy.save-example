# README #

### Description ###

Example of saving arrays with numpy. If the file 
"saved.npy" exists, we reload it. If it doesn't, we 
create it. 

In many cases where we have to do complicated analysis, 
it will be much quicker to save and reload a file than 
to reanalyse the data every time. For example, if you're
messing with some plots and the analysis takes a minute 
each time you adjust the plot, you can save a load of time
by only running the analysis when necessary. In this 
case it only takes 7 seconds to create the array but it's 
still much faster to load it from an .npy instead. 

### Who do I talk to? ###

Speak to Jack if it doesn't work for some reason. 