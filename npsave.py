#! /usr/bin/env python
# The above line means that we can run the file by doing 
# ./filename.py (provided we've done chmod +x filename.py 
# to give it execute permissions). 

# Example of saving arrays with numpy. If the file 
# "saved.npy" exists, we reload it. If it doesn't, we 
# create it. 

# In many cases where we have to do complicated analysis, 
# it will be much quicker to save and reload a file than 
# to reanalyse the data every time. For example, if you're
# messing with some plots and the analysis takes a minute 
# each time you adjust the plot, you can save a load of time
# by only running the analysis when necessary. In this 
# case it only takes 7 seconds to create the array but it's 
# still much faster to load it from an .npy instead. 

import numpy as np
from time import time   # to count time intervals
from os import stat     # for file sizes

def make_array():
    # Make some junk data. 
    x = np.random.normal(scale=np.pi, size=(5000, 5000))
    x *= np.random.normal(scale=1.0, size=(5000, 5000))
    x *= np.random.normal(scale=1.0, size=(5000, 5000))
    x *= np.random.normal(scale=1.0, size=(5000, 5000))
    x = np.sin(x)
    return x

try: 
    # Try to load the .npy file. If there is an error 
    # we get moved to the "except" block. We assume the 
    # error is caused by the "saved.npy" file being missing. 

    ta = time()
    x = np.load("saved.npy")
    tb = time()
    print("Loading from .npy took", tb - ta, "s")   

except:
    # If there's an error above, we need to create the array and save it 
    # to an .npy file. 

    # Make the array
    ta = time()
    x = make_array()
    tb = time()
    print("Making array took", tb - ta, "s")

    # Save it to an .npy
    np.save("saved.npy", x)
    ta = time()
    print("Saving to .npy took", ta - tb, "s")

    # Save it to a .csv
    np.savetxt("saved.csv", x)
    tb = time()
    print("Saving to .csv took", tb - ta, "s")

    # Compare the file sizes
    csvs = stat("saved.csv").st_size
    npys = stat("saved.npy").st_size
    print(".csv file is", csvs/npys, "times bigger than .npy file")
    
    # Print some stuff out to check the data's the same
    print("Mean:", np.mean(x))

    # Reload the .npy
    x = np.load("saved.npy")
    ta = time()
    print("Reloading from .npy took", ta - tb, "s")

    # Reload the .csv
    x = np.loadtxt("saved.csv")
    tb = time()
    print("Reloading from .csv took", tb - ta, "s")

# Print some stuff out to check the data's the same
print("Mean:", np.mean(x))
